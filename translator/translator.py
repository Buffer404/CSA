from utility import *
import functions
from isa.isa import Term, write_code
from isa import isa_const


def translate(text):
    """Транслируем текст в последовательность значимых термов."""

    for line_num, line in enumerate(text.split('\n'), 1):
        for pos, word in enumerate(line.split(), 1):
            if (word in isa_const.correct_words) or (is_num(word) == 1) or (is_word(word) == 1):
                isa_const.terms.append(Term(pos, word))

    """Проверяем корректность программы: скобки должны быть парными."""

    for term in isa_const.terms:
        if term.symbol == '(':
            isa_const.deep += 1
        if term.symbol == ')':
            isa_const.deep -= 1
        assert isa_const.deep >= 0, "Unbalanced brackets!"
    assert isa_const.deep == 0, "Unbalanced brackets!"

    """Трансляция в ассемблер"""

    while isa_const.term_number != len(isa_const.terms):
        if isa_const.terms[isa_const.term_number].symbol == '(':
            isa_const.deep += 1
            isa_const.term_number += 1
            choose_funk()
            if len(isa_const.terms) == isa_const.term_number:
                break
            isa_const.term_number += 1
        elif isa_const.terms[isa_const.term_number].symbol == ')':
            isa_const.deep -= 1
            isa_const.term_number += 1
    isa_const.code.append({'opcode': "halt", 'term': len(isa_const.code)})

    return isa_const.code


def choose_funk():
    """Обработка текущей функции"""
    match isa_const.terms[isa_const.term_number].symbol:
        case ')':
            functions.write_close_bracket()
        case '(':
            functions.write_open_bracket()
        case "defvar":
            functions.write_defvar()
        case "dotimes":
            functions.write_dotimes()
        case "cond":
            functions.write_cond()
        case "=":
            functions.write_equally()
        case "mod":
            functions.write_mod()
        case "setq":
            functions.write_setq()
        case '+' | '-' | '/' | '*':
            functions.write_alu(isa_const.terms[isa_const.term_number].symbol)
        case "print":
            functions.write_print()
        case "format":
            functions.write_format()
        case "loop":
            functions.write_loop()
        case "read":
            functions.write_read()


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"
    source, target = args

    with open(source, "rt", encoding="utf-8") as f:
        source = f.read()
    code = translate(source)
    print("source LoC:", len(source.split()), "code instr:", len(code))
    write_code(target, code)


if __name__ == '__main__':
    main([".\examples\cat.lisp", "target.out"])
