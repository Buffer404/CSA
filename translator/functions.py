from isa import isa_const
from utility import *
from translator import choose_funk
from isa.isa_const import *


def write_close_bracket():
    isa_const.deep -= 1
    isa_const.term_number += 1


def write_open_bracket():
    isa_const.deep += 1
    isa_const.term_number += 1
    choose_funk()


def write_defvar():
    var = get_args()
    memory_map[var] = len(code)
    val = get_args()
    if val == ')':
        val = ' '
    code.append({'opcode': "movv", 'arg': [memory_map[var], val], 'term': len(code)})


def write_dotimes():
    stack.extend([terms[isa_const.term_number + 2].symbol, terms[isa_const.term_number + 3].symbol, len(code)])
    isa_const.term_number += 5
    choose_funk()
    idx = stack.pop(-2) - 1
    begin = code[idx]
    begin['arg'][0] = (len(code) - 1)
    code[idx] = (begin)
    idx = stack.pop() - 1
    m = ({'opcode': "loop", 'arg': [stack.pop(), memory_map[stack.pop()], memory_map[stack.pop()]],
          'term': len(code) - 1})
    code[idx] = m


def write_cond():
    idx = 0
    isa_const.term_number += 1
    cur_deep = isa_const.deep  # 2
    while isa_const.deep >= cur_deep:
        choose_funk()
        if isa_const.deep == cur_deep:
            idx = stack.pop()
            begin = code[idx]
            begin["arg"][0] = len(code) + 1
            code[idx] = begin
            code.append({'opcode': "jp", 'arg': [0], 'term': len(code)})
            stack.append(len(code))
            isa_const.term_number += 1
    command = code[idx]
    command['arg'][0] = len(code) - 1
    code[idx] = command


def write_equally():
    get_args()
    code.append({'opcode': "bne", 'arg': [[get_args()]], 'term': len(code)})
    stack.append(len(code) - 1)
    isa_const.term_number += 1


def write_mod():
    arg1 = memory_map[get_args()]
    code.append({'opcode': "mod", 'arg': [arg1, get_args()], 'term': len(code)})
    isa_const.term_number += 1
    isa_const.deep -= 1


def write_setq():
    val = get_args()
    get_args()
    if code[len(code) - 1]["opcode"] != "rd":
        code.append({'opcode': "mov", 'arg': [memory_map[val]], 'term': len(code)})
    else:
        rd = code.pop()
        rd["arg"] = [memory_map[val]]
        code.append(rd)


def write_alu(op):
    code.append({'opcode': isa_const.symbol2opcode.get(op).value, 'arg': [memory_map[get_args()],
                                                                          memory_map[get_args()]], 'term': len(code)})
    isa_const.term_number += 1
    isa_const.deep -= 1


def write_print():
    code.append({'opcode': "print", 'arg': [memory_map[get_args()]], 'term': len(code)})


def write_format():
    get_args()
    code.append({'opcode': "printf", 'arg': [memory_map[get_args()]], 'term': len(code)})


def write_loop():
    stack.append(len(code))
    cur_deep = isa_const.deep
    isa_const.term_number += 1
    while cur_deep <= isa_const.deep:
        choose_funk()
        isa_const.term_number += 1
        if len(terms) == isa_const.term_number:
            break
    i = stack.pop()
    code.append({'opcode': "jp", 'arg': [i], 'term': len(code)})


def write_read():
    if code[len(code) - 1]["opcode"] == "setq":
        code.pop()
    code.append({'opcode': "rd", 'arg': [], 'term': len(code)})
    isa_const.deep -= 1
    isa_const.term_number += 1


def get_args():
    """Получение аргументов для функции"""
    isa_const.term_number += 1
    if terms[isa_const.term_number].symbol == '(':
        isa_const.term_number += 1
        isa_const.deep += 1
        choose_funk()
    elif is_num(terms[isa_const.term_number].symbol):
        return int(terms[isa_const.term_number].symbol)
    else:
        return terms[isa_const.term_number].symbol
